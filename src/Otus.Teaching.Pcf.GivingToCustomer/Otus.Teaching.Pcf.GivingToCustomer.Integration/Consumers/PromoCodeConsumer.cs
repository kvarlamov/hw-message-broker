﻿using System;
using System.Threading.Tasks;
using Contract;
using MassTransit;
using MassTransit.ConsumeConfigurators;
using MassTransit.Definition;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration.Consumers
{
    public class PromoCodeConsumer : IConsumer<PromocodeContract>
    {
        private readonly IGivePromoCodesToCustomersService _service;

        public PromoCodeConsumer(IGivePromoCodesToCustomersService service)
        {
            _service = service;
        }
        
        public async Task Consume(ConsumeContext<PromocodeContract> context)
        {
            var data = context.Message;

            if (data == null)
            {
                throw new Exception("null");
            }

            var request = new GivePromoCodeRequest
            {
                ServiceInfo = data.ServiceInfo,
                PartnerId = data.PartnerId,
                PromoCodeId = data.PromoCodeId,
                PromoCode = data.PromoCode,
                BeginDate = data.BeginDate,
                EndDate = data.EndDate,
                PreferenceId = data.PreferenceId
            };

            await _service.GivePromoCodesToCustomersWithPreferenceAsync(request);
        }
    }

    public class PromoCodeConsumerDefinition : ConsumerDefinition<PromoCodeConsumer>
    {
        public PromoCodeConsumerDefinition()
        {
            EndpointName = "promocode-service";

            ConcurrentMessageLimit = 8;
        }
    }
}
﻿using System.Data;
using System.Threading.Tasks;
using Contract;
using MassTransit;
using Otus.Teaching.Pcf.Administration.Core.Abstractions;

namespace Otus.Teaching.Pcf.Administration.Core.Integration
{
    public class NotifyAdminConsumer : IConsumer<NotifyAdminContract>
    {
        private readonly IEmployeeService _notifyService;

        public NotifyAdminConsumer(IEmployeeService notifyService)
        {
            _notifyService = notifyService;
        }
        
        public async Task Consume(ConsumeContext<NotifyAdminContract> context)
        {
            var data = context.Message;

            if (data == null)
            {
                throw new NoNullAllowedException();
            }

            await _notifyService.UpdateAppliedPromocodes(data.Id);
        }
    }
}
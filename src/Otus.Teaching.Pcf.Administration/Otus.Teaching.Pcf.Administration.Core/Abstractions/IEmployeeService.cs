﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions
{
    public interface IEmployeeService
    {
        Task UpdateAppliedPromocodes(Guid id);
    }
}
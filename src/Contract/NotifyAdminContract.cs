﻿using System;

namespace Contract
{
    public interface NotifyAdminContract
    {
        public Guid Id { get; set; }
    }
}
﻿using System;
using System.Threading.Tasks;
using Contract;
using MassTransit;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class MassTransitService : IMassTransitService
    {
        private readonly IPublishEndpoint _publishEndpoint;

        public MassTransitService(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }
        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            var dto = new GivePromoCodeToCustomerDto()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            };

            await _publishEndpoint.Publish<PromocodeContract>(dto);
        }

        public async Task NotifyAdmin(Guid requestPartnerManagerId)
        {
            var dto = new RequestPartnerManagerDto(requestPartnerManagerId);
            await _publishEndpoint.Publish<NotifyAdminContract>(dto);
        }
    }
}
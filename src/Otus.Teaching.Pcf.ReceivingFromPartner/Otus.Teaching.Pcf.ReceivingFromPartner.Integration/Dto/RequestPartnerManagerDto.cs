﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto
{
    public class RequestPartnerManagerDto
    {
        public Guid Id { get; }
        public RequestPartnerManagerDto(Guid id)
        {
            Id = id;
        }
    }
}